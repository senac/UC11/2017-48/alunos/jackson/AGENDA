package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import model.Agenda;

public class AgendaDAO extends SQLiteOpenHelper {

    private static final String DATABASE = "zimerman.agenda.Agenda";
    private static final int VERSAO = 1;


    public AgendaDAO(Context context ) {
        super(context, DATABASE,null,VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    /*nome,  endereço , telefone , celular e foto*/
        String ddl   =  "CREATE TABLE  zimerman.agenda.Agenda (" +
                "id Integer PRIMARY KEY ," +
                "nome TEXT NOT NULL ," +
                "endereco TEXT ," +
                "telefone TEXT ," +
                "celular TEXT , " +
                "foto TEXT , " +
                ");";

        db.execSQL(ddl);

    }

    public void salvar (Agenda agenda){

        ContentValues values = new ContentValues();
        values.put("nome",agenda.getNome() );
        values.put("endereco" , agenda.getEndereco());
        values.put("telefone", agenda.getTelefone());
        values.put("celular", agenda.getCelular());
        values.put("foto", agenda.getFoto());

        if (agenda.getId() == 0){
            getWritableDatabase().insert("Agenda",null,values);

        }else {
            getWritableDatabase().update("Agenda", values,"id =" + agenda.getId(), null);
        }



    }

        public List<Agenda> getLista(){
            List<Agenda> lista = new ArrayList<>();

            String [] colunas  = {"id", "nome", "endereco", "telefone", "celular", "foto"};

            Cursor cursor = getWritableDatabase().query("Agenda",
                    colunas, null, null, null,
                    null, null);

            while (cursor.moveToNext()){
                Agenda agenda = new Agenda();
                agenda.setId(cursor.getInt(0));
                agenda.setNome(cursor.getString(1));
                agenda.setEndereco(cursor.getString(2));
                agenda.setTelefone(cursor.getString(3));
                agenda.setCelular(cursor.getString(4));
                agenda.setFoto(cursor.getString(5));

                lista.add(agenda);


            }

            return lista;

        }



    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

            String ddl = "DROP TABLE IF EXISTS Agenda ; " ;
            db.execSQL(ddl);
            this.onCreate(db);
    }
}
