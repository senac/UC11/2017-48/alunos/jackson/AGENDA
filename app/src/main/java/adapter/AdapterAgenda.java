package adapter;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import model.Agenda;
import zimerman.agenda.R;

public class AdapterAgenda extends BaseAdapter {

    private List<Agenda> lista;
    private Activity contexto;

    public AdapterAgenda(Activity contexto, List<Agenda> lista) {
        this.contexto = contexto ;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int indice) {
        return this.lista.get(indice);


    }

    @Override
    public long getItemId(int id) {
      int posicao =  0;

      for (int i  = 0 ; i < this.lista.size(); i++) {
          if (this.lista.get(i).getId() == id){
              posicao = 1;
                      break;
          }
      }
      return posicao;
    }

    @Override
    public View getView(int posicao, View convertView, ViewGroup parent) {

        View view = contexto.getLayoutInflater()
                .inflate(R.layout.activity_agenda_lista, parent, false);

        ImageView imageView = view.findViewById(R.id.foto);
        TextView textViewNome = view.findViewById(R.id.nome);


        Agenda agenda  = this.lista.get(posicao);


        textViewNome.setText(agenda.getNome());

        return view;
    }
}
