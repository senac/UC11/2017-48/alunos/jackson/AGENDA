package zimerman.agenda;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import dao.AgendaDAO;
import model.Agenda;

public class CadastroActivity extends AppCompatActivity {


    public  static final int  REQUEST_IMAGE_CAPTURE = 1;

    /*nome,  endereço , telefone , celular e foto*/
    private ImageView imageView;
    private EditText editTextNome;
    private EditText editTextEndereco;
    private EditText editTextTelefone;
    private EditText editTextCelular;
    private Button buttonSalvar;

    private String caminhoArquivo;

    private Agenda agenda;
    private AgendaDAO dao;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        imageView = findViewById(R.id.foto);
        editTextNome = findViewById(R.id.nome);
        editTextEndereco = findViewById(R.id.endereco);
        editTextTelefone = findViewById(R.id.telefone);
        editTextCelular = findViewById(R.id.celular);
        buttonSalvar = findViewById(R.id.btnsalvar);



    }

    public void capturarImagem(View view) {
        caminhoArquivo = Environment
                .getExternalStorageDirectory().toString() + "/agenda/" + System.currentTimeMillis() + ".png";

        File arquivo = new File(caminhoArquivo);

        Uri localImagem = Uri.fromFile(arquivo);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, localImagem);

        if (intent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE );
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            agenda.setFoto(caminhoArquivo);

            Bitmap imagem = BitmapFactory.decodeFile(agenda.getFoto());
            Bitmap imageReduzida = Bitmap.createScaledBitmap(imagem, 200, 200, true);

            imageView.setImageBitmap(imageReduzida);
        }

    }

    private boolean IsPreenchido (String valeu){
        return (valeu != null && !valeu.isEmpty());
    }

    public void validarDados() throws Exception {
        List<String> listaCamposRequeridos = new ArrayList<>();

        if (!IsPreenchido(agenda.getNome())){
            listaCamposRequeridos.add("Nome");
            editTextNome.setBackgroundColor(getResources().getColor(R.color.vermelho));
        }

        if (!IsPreenchido(agenda.getEndereco())){
            listaCamposRequeridos.add("Endereco");
                    }

        if (!IsPreenchido(agenda.getTelefone())){
            listaCamposRequeridos.add("Telefone");
        }
        if (!IsPreenchido(agenda.getCelular())){
            listaCamposRequeridos.add("Celular");
        }

        if (listaCamposRequeridos.size() > 0 ){
            throw new Exception("Campos requeridos (s)" + listaCamposRequeridos.toString());
        }
    }


    public void salvar(View view) {

        try {

            String nome = editTextNome.getText().toString();
            String endereco = editTextEndereco.getText().toString();
            String telefone = editTextTelefone.getText().toString();
            String celular = editTextCelular.getText().toString();

            Bitmap bmp = imageView.getDrawingCache();


            agenda = new Agenda();
            agenda.setNome(nome);
            agenda.setEndereco(endereco);
            agenda.setTelefone(telefone);
            agenda.setCelular(celular);
            agenda.setFoto(caminhoArquivo);

            this.validarDados();


            dao = new AgendaDAO(this);
            dao.salvar(agenda);
            dao.close();

            setResult(RESULT_OK);

            finish();

        } catch (Exception ex) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Erro Validação")
                    .setMessage(ex.getMessage());

            AlertDialog dialog = builder.create();
            dialog.show();


        }

    }
}
